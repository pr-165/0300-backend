const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const ecoleSchema = new Schema({
    "adresse": String,
    "nom": String,
    "localite": String,
    "npa": Number,
    "moniteurs": Array,
}, { collection: "ecole" })

module.exports = model("ecole", ecoleSchema);
