const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const sessionSchema = new Schema({
    "date": Date,
    "dureeM": Number,
    "places": Number,
    "participations": Array,
    "cours_id": String,
    "moniteur_id": String
}, { collection: "session" })

module.exports = model("session", sessionSchema);
