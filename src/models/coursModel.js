const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const coursSchema = new Schema({
    "tarif": Number,
    "niveau": String,
    "type": String,
}, { collection: "cours"})

module.exports = model("cours", coursSchema);