const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const proprietaireSchema = new Schema({
    "adresse": String,
    "certificatDelivre": Boolean,
    "dateNaissance": Date,
    "nom": String,
    "origine": String,
    "prenom": String,
    "chiens": Array,
    "localite": String,
    "npa": Number,
    "mails": Array,
    "telephones": Array,
}, { collection: "proprietaire" })

module.exports = model("proprietaire", proprietaireSchema);
