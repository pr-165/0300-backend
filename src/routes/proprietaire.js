const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

router.use(express.urlencoded({extended:true}))
router.use(express.json())

const proprietaireModel = require('../models/proprietaireModel')


// Route Post/Create
router.post('/', async (req, res) => {
    const { adresse, certificatDelivre, dateNaissance, nom, origine, prenom, chiens, localite, npa, mails, telephones } = req.body;
    const nouveauProprietaire = new proprietaireModel({
        adresse: adresse,
        certificatDelivre: certificatDelivre,
        dateNaissance: dateNaissance,
        nom: nom,
        origine: origine,
        prenom: prenom,
        chiens: chiens,
        localite: localite,
        npa: npa,
        mails: mails,
        telephones: telephones,
    });

    try {
        const Proprietaire = await nouveauProprietaire.save();
        res.status(201).send("Proprietaire crée");
    } catch (err) {
        res.status(400).send(err.message);
    }
});

// Route Get/Read
router.get('/:id', async (req, res) => {
    const proprietaireId = req.params.id;

    try {
        const proprietaire = await proprietaireModel.findById(proprietaireId);
        if (!proprietaire) {
            return res.status(404).json({ message: "Proprietaire not found" });
        }
        res.json(proprietaire);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.get('/', async (req, res) => {
    proprietaire = await proprietaireModel.find()
    res.send(proprietaire)
})






// Route Patch/Update
router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const { adresse, certificatDelivre, dateNaissance, nom, origine, prenom, chiens, localite, npa, mails, telephones } = req.body;
    const nouveauProprietaire = new proprietaireModel({
        adresse: adresse,
        certificatDelivre: certificatDelivre,
        dateNaissance: dateNaissance,
        nom: nom,
        origine: origine,
        prenom: prenom,
        chiens: chiens,
        localite: localite,
        npa: npa,
        mails: mails,
        telephones: telephones,
    });

    try {
        const proprietaire = await nouveauProprietaire.findByIdAndUpdate(id, { adresse, certificatDelivre, dateNaissance, nom, origine, prenom, chiens, localite, npa, mails, telephones}, { new: true });
        res.send(proprietaire);
      } catch (error) {
        console.error(error);
        res.status(500).send(error);
      }
});

// Route Delete
router.delete('/:id', async (req, res) => {
    const { adresse, certificatDelivre, dateNaissance, nom, origine, prenom, chiens, localite, npa, mails, telephones } = req.params;
    const nouveauProprietaire = new proprietaireModel({
        adresse: adresse,
        certificatDelivre: certificatDelivre,
        dateNaissance: dateNaissance,
        nom: nom,
        origine: origine,
        prenom: prenom,
        chiens: chiens,
        localite: localite,
        npa: npa,
        mails: mails,
        telephones: telephones,
    });
  
    try {
      const proprietaire = await nouveauProprietaire.findByIdAndDelete({ adresse, certificatDelivre, dateNaissance, nom, origine, prenom, chiens, localite, npa, mails, telephones });
      res.send(proprietaire);
    } catch (error) {
      console.error(error);
      res.status(500).send(error);
    }
  });


module.exports = router
