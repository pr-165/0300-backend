const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()
router.use(express.urlencoded({extended:true}))
router.use(express.json())

const coursModel = require('../models/coursModel')
const ecoleModel = require('../models/ecoleModel')



router.get('/', async (req, res) => {
    cours = await coursModel.find()
    res.send(cours)
})

router.get('/:id', async (req, res) => {
  const coursId = req.params.id;

  try {
      const cours = await coursModel.findById(coursId);
      if (!cours) {
          return res.status(404).json({ message: "Cours pas trouvé" });
      }
      res.json(cours);
  } catch (err) {
      res.status(500).json({ message: err.message });
  }
});

router.post('/save', async (req, res) => {
    const { tarif, niveau, type } = req.body;
    const nouveauCours = new coursModel({
        tarif: tarif,
        niveau: niveau,
        type: type
    });

    try{
        const Cours = await nouveauCours.save()
        res.send("Cours crée")
    } catch(err){
        console.log(err)
    } 
});

router.delete('/delete/:id', async (req, res) => {
    const { tarif } = req.params;
    const nouveauCours = new coursModel({
        tarif: tarif,
        niveau: niveau,
        type: type
    });
  
    try {
      const user = await nouveauCours.findByIdAndDelete(tarif);
      res.send(user);
    } catch (error) {
      console.error(error);
      res.status(500).send(error);
    }
  });

router.put('/update/:id', async (req, res) => {
  const { id } = req.params;
  const { name, email, age } = req.body;
  const nouveauCours = new coursModel({
    tarif: tarif,
    niveau: niveau,
    type: type
});

  try {
    const user = await nouveauCours.findByIdAndUpdate(id, { name, email, age }, { new: true });
    res.send(user);
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
});

router.get('/aggregation/statistics', async (req, res) => {
  try {
      const results = await coursModel.aggregate([
          
          {
              $lookup: {
                  from: "ecoles",
                  localField: "ecoleId",
                  foreignField: "_id",
                  as: "ecoleDetails"
              }
          },
          
          
          { $unwind: "$ecoleDetails" },
          { $unwind: "$ecoleDetails.moniteurs" },

         
          {
              $match: { tarif: { $eq: 10 } }
          },
          
          
          {
              $group: {
                  _id: "$type",
                  totalCours: { $sum: 1 },
                  tarifMoyen: { $avg: "$tarif" },
                  moniteurs: { $addToSet: "$ecoleDetails.moniteurs" }
              }
          },
          
          
          {
              $project: {
                  _id: 0,
                  type: "$_id",
                  totalCours: 1,
                  tarifMoyen: 1,
                  moniteurs: 1
              }
          }
      ]);

      res.json(results);
  } catch (err) {
      res.status(500).json({ message: err.message });
  }
});

module.exports = router